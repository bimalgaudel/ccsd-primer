#include "ccsd.h"
#include <iostream>

double ccsd_energy(size_t ndocc, size_t nao,
        const DTensor& Fock_mat, const DTensor& mo_ints) {
    /*** =========================== ***/
    /***       CCSD Energy           ***/
    /*** =========================== ***/
    //
    // See Stanton's paper: J. Chem. Phys., Vol. 94, No. 6, 15 March 1991
    std::cout << "\nRunning CCSD code...\n";
    const auto nvirt = nao - ndocc;
    using btas::Range;
    using btas::Range1;
    using btas::contract;

    // 
    // setting up the intermediates
    //
    // D_ov and D_oovv
    DTensor D_ov(ndocc, nvirt);
    DTensor D_oovv(ndocc, ndocc, nvirt, nvirt);
    // Fock_xx
    DTensor Fock_oo(ndocc, ndocc);
    DTensor Fock_ov(ndocc, nvirt);
    DTensor Fock_vv(nvirt, nvirt);
    // G_xxxx
    DTensor G_oovv(ndocc, ndocc, nvirt, nvirt);
    DTensor G_vvoo(nvirt, nvirt, ndocc, ndocc);
    DTensor G_ovov(ndocc, nvirt, ndocc, nvirt);
    DTensor G_ovvo(ndocc, nvirt, nvirt, ndocc);
    for (auto i = 0; i < ndocc; ++i) {
        Fock_oo(i,i) = Fock_mat(i,i);
        for (auto a = 0; a < nvirt; ++a) {
            Fock_ov(i,a) = Fock_mat(i,a+ndocc);
            D_ov(i,a) = Fock_mat(i,i) - Fock_mat(a+ndocc,a+ndocc);
            for (auto j = 0; j < ndocc; ++j) {
                for (auto b = 0; b < nvirt; ++b) {
                    Fock_vv(a,b) = Fock_mat(a+ndocc,b+ndocc);
                    D_oovv(i,j,a,b) = D_ov(i,a) + Fock_mat(j,j) - Fock_mat(b+ndocc,b+ndocc);
                    G_oovv(i,j,a,b) = mo_ints(i,j,a+ndocc,b+ndocc);
                    G_vvoo(a,b,i,j) = mo_ints(a+ndocc,b+ndocc,i,j);
                    G_ovov(i,a,j,b) = mo_ints(i,a+ndocc,j,b+ndocc);
                    G_ovvo(i,a,b,j) = mo_ints(i,a+ndocc,b+ndocc,j);
                }
            }
        }
    }
    // more intermediates
    DTensor G_ovvv(ndocc, nvirt, nvirt, nvirt);
    for (auto i = 0; i < ndocc; ++i) {
        for (auto a = 0; a < nvirt; ++a) {
            for (auto b = 0; b < nvirt; ++b) {
                for (auto c = 0; c < nvirt; ++c) {
                    G_ovvv(i,a,b,c) = mo_ints(i,a+ndocc,b+ndocc,c+ndocc);
                }
            }
        }
    }
    DTensor G_ooov(ndocc, ndocc, ndocc, nvirt);
    for (auto i = 0; i < ndocc; ++i) {
        for (auto j = 0; j < ndocc; ++j) {
            for (auto k = 0; k < ndocc; ++k) {
                for (auto a = 0; a < nvirt; ++a) {
                    G_ooov(i,j,k,a) = mo_ints(i,j,k,a+ndocc);
                }
            }
        }
    }
    DTensor G_oooo(ndocc, ndocc, ndocc, ndocc);
    for (auto i = 0; i < ndocc; ++i) {
        for (auto j = 0; j < ndocc; ++j) {
            for (auto k = 0; k < ndocc; ++k) {
                for (auto l = 0; l < ndocc; ++l) {
                    G_oooo(i,j,k,l) = mo_ints(i,j,k,l);
                }
            }
        }
    }
    DTensor G_vvvv(nvirt, nvirt, nvirt, nvirt);
    for (auto a = 0; a < nvirt; ++a) {
        for (auto b = 0; b < nvirt; ++b) {
            for (auto c = 0; c < nvirt; ++c) {
                for (auto d = 0; d < nvirt; ++d) {
                    G_vvvv(a,b,c,d) = mo_ints(a+ndocc,b+ndocc,c+ndocc,d+ndocc);
                }
            }
        }
    }
    /* using std::cout; */
    /* using std::endl; */
    /* cout << "--------------------------------------------------" << endl; */
    /* cout << "Printing the norms of set up tensors" << endl; */
    /* cout << sqrt(btas::dot(Fock_oo,Fock_oo)) << " Fock_oo" << endl; */
    /* cout << sqrt(btas::dot(Fock_ov,Fock_ov)) << " Fock_ov" << endl; */
    /* cout << sqrt(btas::dot(Fock_vv,Fock_vv)) << " Fock_vv" << endl; */
    /* cout << sqrt(btas::dot(G_oooo,G_oooo))   << " G_oooo" << endl; */
    /* cout << sqrt(btas::dot(G_vvvv,G_vvvv))   << " G_vvvv" << endl; */
    /* cout << sqrt(btas::dot(G_ovvv,G_ovvv))   << " G_ovvv" << endl; */
    /* cout << sqrt(btas::dot(G_ooov,G_ooov))   << " G_ooov" << endl; */
    /* cout << sqrt(btas::dot(G_oovv,G_oovv))   << " G_oovv" << endl; */
    /* cout << sqrt(btas::dot(G_ovov,G_ovov))   << " G_ovov" << endl; */
    /* cout << sqrt(btas::dot(D_ov,D_ov)) << " D_ov" << endl; */
    /* cout << sqrt(btas::dot(D_oovv,D_oovv)) << " D_oovv" << endl; */
    /* cout << "--------------------------------------------------" << endl; */

    // finished generating intermediates
    //////////////////

    // setting up initial t_ov
    DTensor t_ov(ndocc, nvirt);
    t_ov.fill(0);
    // setting up initial t_oovv
    DTensor t_oovv(ndocc, ndocc, nvirt, nvirt);
    t_oovv.fill(0);
    /* cout << sqrt(btas::dot(t_ov,t_ov)) << " t_ov" << endl; */
    /* cout << sqrt(btas::dot(t_oovv,t_oovv)) << " t_oovv" << endl; */

    //////////////////
    // start loop here
    //////////////////

    const auto conv = 1e-12;
    auto e_ccsd = 0.0;
    auto normdiff = 0.0;
    auto ediff = 0.0;
    const auto maxiter = 100;
    auto iter = 0;

    do {
        ++iter;
        // forming the effective two-particle excitation operators
        DTensor tau(ndocc,ndocc,nvirt,nvirt);
        DTensor tau_tilde(ndocc,ndocc,nvirt,nvirt);
        for (auto i = 0; i < ndocc; ++i) {
            for (auto j = 0; j < ndocc; ++j) {
                for (auto a = 0; a < nvirt; ++a) {
                    for (auto b = 0; b < nvirt; ++b) {
                        tau(i,j,a,b) = t_oovv(i,j,a,b)
                                      + t_ov(i,a)*t_ov(j,b)
                                      - t_ov(i,b)*t_ov(j,a);
                        //
                        tau_tilde(i,j,a,b) = t_oovv(i,j,a,b)
                                      + 0.5*t_ov(i,a)*t_ov(j,b)
                                      - 0.5*t_ov(i,b)*t_ov(j,a);
                    }
                }
            }
        }
        // enums for labeling tensor dimensions
        enum {a,b,e,f,i,j,m,n};
        // a buffer tensor to hold calculations
        DTensor temp_tensor1;

        // Generating intermediates
        // F_ae
        DTensor F_ae(nvirt,nvirt);
        for (auto a = 0; a < nvirt; ++a) {
            for (auto e = 0; e < nvirt; ++e) {
                F_ae(a,e) = (a != e) ? Fock_vv(a,e) : 0.0;
            }
        }
        contract(-0.5, Fock_ov, {m,e}, t_ov, {m,a}, 1.0, F_ae, {a,e});
        contract( 1.0, t_ov, {m,f}, G_ovvv, {m,a,f,e}, 1.0, F_ae, {a,e});
        contract(-0.5, tau_tilde, {m,n,a,f}, G_oovv, {m,n,e,f}, 1.0, F_ae, {a,e});
        //
         
        // F_mi
        DTensor F_mi(ndocc,ndocc);
        for (auto m = 0; m < ndocc; ++m) {
            for (auto i = 0; i < ndocc; ++i) {
                F_mi(m,i) = (m != i) ? Fock_oo(m,i) : 0.0;
            }
        }
        contract(0.5, t_ov, {i,e}, Fock_ov, {m,e}, 1.0, F_mi, {m,i});
        contract(1.0, t_ov, {n,e}, G_ooov, {m,n,i,e}, 1.0, F_mi, {m,i});
        contract(0.5, tau_tilde, {i,n,e,f}, G_oovv, {m,n,e,f}, 1.0, F_mi, {m,i});
        //
         
        // F_me
        auto F_me = Fock_ov;
        contract(1.0, t_ov, {n,f}, G_oovv, {m,n,e,f}, 1.0, F_me, {m,e});
        //
        
        // W_mnij
        auto W_mnij = G_oooo;
        contract( 1.0, t_ov, {j,e}, G_ooov, {m,n,i,e}, 1.0, W_mnij, {m,n,i,j});
        contract(-1.0, t_ov, {j,e}, G_ooov, {m,n,i,e}, 1.0, W_mnij, {m,n,j,i});
        contract(0.25, tau, {i,j,e,f}, G_oovv, {m,n,e,f}, 1.0, W_mnij, {m,n,i,j});
        //

        // W_abef
        auto W_abef = G_vvvv;
        contract(-1.0, t_ov, {m,b}, G_ovvv, {m,a,f,e}, 1.0, W_abef, {a,b,e,f});
        contract( 1.0, t_ov, {m,b}, G_ovvv, {m,a,f,e}, 1.0, W_abef, {b,a,e,f});
        contract(0.25, tau, {m,n,a,b}, G_oovv, {m,n,e,f}, 1.0, W_abef, {a,b,e,f});
        //
        
        // W_mbej
        auto W_mbej = G_ovvo;
        contract( 1.0, t_ov, {j,f}, G_ovvv, {m,b,e,f}, 1.0, W_mbej, {m,b,e,j});
        contract(-1.0, t_ov, {n,b}, G_ooov, {n,m,j,e}, 1.0, W_mbej, {m,b,e,j});
        contract(-0.5, t_oovv, {j,n,f,b}, G_oovv, {m,n,e,f}, 1.0, W_mbej, {m,b,e,j});
        temp_tensor1.resize(Range{Range1{ndocc}, Range1{nvirt}, Range1{nvirt}, Range1{nvirt}});
        contract(-1.0, t_ov, {n,b}, G_oovv, {m,n,e,f}, 0.0, temp_tensor1, {m,b,e,f});
        contract( 1.0, temp_tensor1, {m,b,e,f}, t_ov, {j,f}, 1.0, W_mbej, {m,b,e,j});
        // finished generating intermediates

        //
        //////////////
        // Implementing T1 equation
        // rhs
        auto R_1 = Fock_ov;
        contract( 1.0, t_ov, {i,e}, F_ae, {a,e}, 1.0, R_1, {i,a});
        contract(-1.0, t_ov, {m,a}, F_mi, {m,i}, 1.0, R_1, {i,a});
        contract( 1.0, t_oovv, {i,m,a,e}, F_me, {m,e}, 1.0, R_1, {i,a});
        contract(-1.0, t_ov, {n,f}, G_ovov, {n,a,i,f}, 1.0, R_1, {i,a});
        /* std::cout << "norm before contraction = " << std::sqrt(btas::dot(R_1, R_1)) << std::endl; */
        contract(-0.5, t_oovv, {i,m,e,f}, G_ovvv, {m,a,e,f}, 1.0, R_1, {i,a});
        /* std::cout << "norm after first contraction = " << std::sqrt(btas::dot(R_1, R_1)) << std::endl; */
        contract(-0.5, t_oovv, {m,n,a,e}, G_ooov, {m,n,i,e}, 1.0, R_1, {i,a});
        /* std::cout << "norm after second contraction = " << std::sqrt(btas::dot(R_1, R_1)) << std::endl; */
        // lhs
        for (auto i = 0; i < ndocc; ++i) {
            for (auto a = 0; a < nvirt; ++a) {
                R_1(i,a) -= t_ov(i,a)*D_ov(i,a);
            }
        }
        //
        

        //
        //////////////
        // Implementing T2 equation
        // rhs #1
        auto R_2 = G_oovv;
        // rhs #2
        contract( 1.0, t_oovv, {i,j,a,e}, F_ae, {b,e}, 1.0, R_2, {i,j,a,b});
        contract(-1.0, t_oovv, {i,j,a,e}, F_ae, {b,e}, 1.0, R_2, {i,j,b,a});
        //
        temp_tensor1.resize(Range{Range1{nvirt}, Range1{nvirt}});
        contract(-0.5, t_ov, {m,b}, F_me, {m,e}, 0.0, temp_tensor1, {e,b});
        contract( 1.0, t_oovv, {i,j,a,e}, temp_tensor1, {e,b}, 1.0, R_2, {i,j,a,b});
        contract(-1.0, t_oovv, {i,j,a,e}, temp_tensor1, {e,b}, 1.0, R_2, {i,j,b,a});
        // rhs #3
        contract(-1.0, t_oovv, {i,m,a,b}, F_mi, {m,j}, 1.0, R_2, {i,j,a,b});
        contract( 1.0, t_oovv, {i,m,a,b}, F_mi, {m,j}, 1.0, R_2, {j,i,a,b});
        //
        temp_tensor1.resize(Range{Range1{ndocc}, Range1{ndocc}});
        contract( 0.5, t_ov, {j,e}, F_me, {m,e}, 0.0, temp_tensor1, {j,m});
        contract(-1.0, t_oovv, {i,m,a,b}, temp_tensor1, {j,m}, 1.0, R_2, {i,j,a,b});
        contract( 1.0, t_oovv, {i,m,a,b}, temp_tensor1, {j,m}, 1.0, R_2, {j,i,a,b});
        // rhs #4
        contract( 0.5, tau, {m,n,a,b}, W_mnij, {m,n,i,j}, 1.0, R_2, {i,j,a,b});
        // rhs #5
        contract( 0.5, tau, {i,j,e,f}, W_abef, {a,b,e,f}, 1.0, R_2, {i,j,a,b});
        // rhs #6
        contract( 1.0, t_oovv, {i,m,a,e}, W_mbej, {m,b,e,j}, 1.0, R_2, {i,j,a,b});
        contract(-1.0, t_oovv, {i,m,a,e}, W_mbej, {m,b,e,j}, 1.0, R_2, {j,i,a,b});
        contract(-1.0, t_oovv, {i,m,a,e}, W_mbej, {m,b,e,j}, 1.0, R_2, {i,j,b,a});
        contract( 1.0, t_oovv, {i,m,a,e}, W_mbej, {m,b,e,j}, 1.0, R_2, {j,i,b,a});
        //
        temp_tensor1.resize(Range{Range1{nvirt}, Range1{nvirt}, Range1{nvirt}, Range1{ndocc}});
        contract(-1.0, t_ov, {m,a}, G_ovvo, {m,b,e,j}, 0.0, temp_tensor1, {a,b,e,j});
        contract( 1.0, t_ov, {i,e}, temp_tensor1, {a,b,e,j}, 1.0, R_2, {i,j,a,b});
        contract(-1.0, t_ov, {i,e}, temp_tensor1, {a,b,e,j}, 1.0, R_2, {j,i,a,b});
        contract(-1.0, t_ov, {i,e}, temp_tensor1, {a,b,e,j}, 1.0, R_2, {i,j,b,a});
        contract( 1.0, t_ov, {i,e}, temp_tensor1, {a,b,e,j}, 1.0, R_2, {j,i,b,a});
        // rhs #7
        contract( 1.0, t_ov, {i,e}, G_ovvv, {j,e,b,a}, 1.0, R_2, {i,j,a,b});
        contract(-1.0, t_ov, {i,e}, G_ovvv, {j,e,b,a}, 1.0, R_2, {j,i,a,b});
        // rhs #8
        contract(-1.0, t_ov, {m,a}, G_ooov, {i,j,m,b}, 1.0, R_2, {i,j,a,b});
        contract( 1.0, t_ov, {m,a}, G_ooov, {i,j,m,b}, 1.0, R_2, {i,j,b,a});
        // lhs
        for (auto i = 0; i < ndocc; ++i) {
            for (auto j = 0; j < ndocc; ++j) {
                for (auto a = 0; a < nvirt; ++a) {
                    for (auto b = 0; b < nvirt; ++b) {
                        R_2(i,j,a,b) -= t_oovv(i,j,a,b)*D_oovv(i,j,a,b);
                    }
                }
            }
        }
        //

        std::cout << std::endl;
        std::cout << "norm(r1) = " << sqrt(btas::dot(R_1, R_1)) << std::endl;
        std::cout << "norm(r2) = " << sqrt(btas::dot(R_2, R_2)) << std::endl;

        // save previous norms
        auto norm_last  = sqrt(btas::dot(t_oovv, t_oovv));
        /* std::cout << "norm_last = " << norm_last << std::endl; */


        //////////////
        // Updating amplitudes
        // update t_ov
        for (auto i = 0; i < ndocc; ++i) {
            for (auto a = 0; a < nvirt; ++a) {
                t_ov(i, a) += R_1(i, a)/D_ov(i,a);
            }
        }

        // update t_oovv
        for (auto i = 0; i < ndocc; ++i) {
            for (auto j = 0; j < ndocc; ++j) {
                for (auto a = 0; a < nvirt; ++a) {
                    for (auto b = 0; b < nvirt; ++b) {
                        t_oovv(i, j, a, b) += R_2(i, j, a, b)/D_oovv(i,j,a,b);
                    }
                }
            }
        }

        // Δe_ccsd calculation in SPIN basis
        auto e_ccsd_last = e_ccsd;
        temp_tensor1.resize(Range{Range1{ndocc},Range1{nvirt}});
        contract(1.0, G_oovv, {i,j,a,b}, t_ov, {i,a}, 0.0, temp_tensor1, {j,b});
        //
        e_ccsd  = 0.5*btas::dot(temp_tensor1, t_ov)
                + 0.25*btas::dot(G_oovv, t_oovv)
                + btas::dot(Fock_ov, t_ov);
        printf("e_ccsd is: %20.12f\n", e_ccsd);
        //
        normdiff = norm_last - std::sqrt(btas::dot(t_oovv, t_oovv));
        ediff    = e_ccsd_last - e_ccsd;

        // ///////////////////
        // check condition and
        // break the loop here
        // ///////////////////
    } while ((fabs(normdiff) > conv || fabs(ediff) > conv) && (iter < maxiter));
    std::cout << "\nConverged after " << iter << " cycles.\n";
    return e_ccsd;
}
