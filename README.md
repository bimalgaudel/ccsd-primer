CCSD Primer
====

An extension to [mp2-primer](https://github.com/bimalgaudel/mp2-primer) project.

Hartree--Fock part will be copied from [Libint](https://github.com/evaleev/libint) project.
See [hartree-fock.cc](https://github.com/evaleev/libint/blob/master/tests/hartree-fock/hartree-fock.cc) file.
MP2 part will be copied from [MP2-Primer](https://github.com/bimalgaudel/mp2-primer) project.
See [mp2.cc](https://github.com/bimalgaudel/mp2-primer/blob/master/mp2.cc) file.

The master branch is spin based CCSD, whereas the 'spatial-basis' branch is exactly what it sounds like.
