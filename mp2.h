#include <cstddef>
#include <btas/btas.h>
#include "hartree-fock.h"

typedef btas::Tensor<double> DTensor;

DTensor compute_ao_ints(const std::vector<libint2::Shell>& shells);
double int_2e_mo(const Matrix& coff_mat, const DTensor& ao_ints_tensor, size_t p, size_t q, size_t r, size_t s);
DTensor compute_mo_ints(const Matrix& coff_mat, const DTensor& ao_ints_tensor);
