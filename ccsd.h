#include "mp2.h"

double ccsd_energy(size_t ndocc, size_t nao,
        const DTensor& F_mat_spin,
        const DTensor& mo_ints_spin);
